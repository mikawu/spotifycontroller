# SpotifyController

Basic controller for Spotify Windows desktop client. Contains methods to play and pause<br/>
songs, playlists, albums, and albums, as well as several information methods. Only tested<br/>
on Windows 8.1 x64.

Requires **Requests** library to be installed.
